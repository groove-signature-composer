import React from 'react';
import './Container.css';

const Container = function({ children, className }) {
  return (
    <div className={`Container ${className}`}>
      {children}
    </div>
  );
};

export default Container;
