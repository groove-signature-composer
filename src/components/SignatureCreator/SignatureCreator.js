import React, { Component } from 'react';
import SignaturePreview from './SignaturePreview/SignaturePreview';
import SignatureForm from './SignatureForm/SignatureForm';
import './SignatureCreator.css';

class SignatureCreator extends Component {
  constructor(props) {
    super(props);

    // Hard-coded initial state, hopefully some day...
    this.state = {
      name: 'Roberto Dip',
      position: 'Full Stack Developer'
    };
  }

  handleFormChange = e => {
    const { name, value } = e.target;

    this.setState({ ...this.state, [name]: value });
  };

  handleFormSubmit = e => {
    e.preventDefault();
  };

  render() {
    return (
      <div className="SignatureCreator">
        <SignaturePreview data={this.state} />
        <SignatureForm
          onChange={this.handleFormChange}
          onSubmit={this.handleFormSubmit}
          className="SignatureCreator-form"
        />
      </div>
    );
  }
}

export default SignatureCreator;
